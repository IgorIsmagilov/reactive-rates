package com.iismagilov.reactiverates.interactor;

import android.support.annotation.NonNull;

import com.iismagilov.reactiverates.data.RatesDataStorage;
import com.iismagilov.reactiverates.model.Currency;
import com.iismagilov.reactiverates.model.Rates;

import io.reactivex.Observable;
import io.reactivex.functions.Function4;

/**
 * Created by Igor Ismagilov on 23.03.17.
 */

public class CalculateRate {


    @NonNull
    private final RatesDataStorage mRatesDataStorage;

    public CalculateRate(@NonNull RatesDataStorage ratesDataStorage) {
        this.mRatesDataStorage = ratesDataStorage;
    }

    public Observable<Result> execute(@NonNull Observable<Double> amount,
                                      @NonNull Observable<Currency> currencyFrom,
                                      @NonNull Observable<Currency> currencyTo) {
        return Observable.combineLatest(
                amount,
                mRatesDataStorage.getRates(currencyFrom),
                currencyFrom,
                currencyTo,
                new Function4<Double, Rates, Currency, Currency, Result>() {
                    @Override
                    public Result apply(Double amount, Rates rates, Currency currencyFrom, Currency currencyTo) throws Exception {
                        if (currencyFrom.equals(currencyTo)) {
                            return new Result(currencyFrom, currencyTo, amount, 1.0);
                        }

                        if (!rates.getRates().containsKey(currencyTo)) {
                            return new Result(currencyFrom, currencyTo, 0.0, 0.0);
                        }

                        double rate = rates.getRates().get(currencyTo);
                        return new Result(currencyFrom, currencyTo, amount * rate, rate);
                    }
                }
        );
    }

    public static class Result {
        @NonNull
        public final Currency currencyFrom;
        @NonNull
        public final Currency currencyTo;
        public final double   convertedAmount;
        public final double   rate;

        public Result(@NonNull Currency currencyFrom,
                      @NonNull Currency currencyTo,
                      double convertedAmount,
                      double rate) {
            this.currencyFrom = currencyFrom;
            this.currencyTo = currencyTo;
            this.convertedAmount = convertedAmount;
            this.rate = rate;
        }
    }


}

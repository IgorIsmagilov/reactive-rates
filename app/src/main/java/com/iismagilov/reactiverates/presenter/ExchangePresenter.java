package com.iismagilov.reactiverates.presenter;

import android.support.annotation.NonNull;
import android.util.Log;

import com.iismagilov.reactiverates.interactor.CalculateRate;
import com.iismagilov.reactiverates.model.Currency;
import com.iismagilov.reactiverates.view.ExchangeView;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Igor Ismagilov on 23.03.17.
 */

public class ExchangePresenter extends BasePresenter<ExchangeView> {

    @NonNull
    private final CalculateRate calculateRate;

    private final DecimalFormat amountFormat = new DecimalFormat("#.##");

    private final DecimalFormat rateFormat = new DecimalFormat("#.#####");

    private final NumberFormat numberFormat = NumberFormat.getInstance();

    private Disposable calculateRatesForwardDisposable;

    private Disposable calculateRatesInverseDisposable;

    public ExchangePresenter(@NonNull CalculateRate calculateRate) {
        this.calculateRate = calculateRate;
    }

    @Override
    public void attachView(@NonNull ExchangeView view) {
        super.attachView(view);

        view.fromAmountFocusChanges().subscribe(new Consumer<Boolean>() {
            @Override
            public void accept(Boolean hasFocus) throws Exception {
                if (hasFocus) {
                    calculateRatesForward();
                } else {
                    if (calculateRatesForwardDisposable != null) {
                        calculateRatesForwardDisposable.dispose();
                    }
                }
            }
        });

        view.toAmountFocusChanges().subscribe(new Consumer<Boolean>() {
            @Override
            public void accept(Boolean hasFocus) throws Exception {
                if (hasFocus) {
                    calculateRatesInverse();
                } else {
                    if (calculateRatesInverseDisposable != null) {
                        calculateRatesInverseDisposable.dispose();
                    }
                }
            }
        });
    }

    @Override
    public void detachView() {
        if (calculateRatesForwardDisposable != null) {
            calculateRatesForwardDisposable.dispose();
        }

        if (calculateRatesInverseDisposable != null) {
            calculateRatesInverseDisposable.dispose();
        }
        super.detachView();
    }

    private void calculateRatesForward() {
        if (calculateRatesForwardDisposable != null && !calculateRatesForwardDisposable.isDisposed()) {
            return;
        }

        calculateRatesForwardDisposable = calculateRate(
                getView().fromAmountChanges(),
                getView().fromCurrencyChanges(),
                getView().toCurrencyChanges()
        ).subscribe(new Consumer<CalculateRate.Result>() {
            @Override
            public void accept(CalculateRate.Result result) throws Exception {
                getView().setToAmount(amountFormat.format(result.convertedAmount));
                getView().setFromRate("");
                if (result.currencyFrom.equals(result.currencyTo) || result.rate == 0) {
                    getView().setTitleRate("");
                    getView().setToRate("");
                } else {
                    getView().setTitleRate(formatRate(result.currencyFrom, result.currencyTo, result.rate));
                    getView().setToRate(formatRate(result.currencyTo, result.currencyFrom, 1 / result.rate));
                }
            }
        });
    }

    private void calculateRatesInverse() {
        if (calculateRatesInverseDisposable != null && !calculateRatesInverseDisposable.isDisposed()) {
            return;
        }

        calculateRatesInverseDisposable = calculateRate(
                getView().toAmountChanges(),
                getView().toCurrencyChanges(),
                getView().fromCurrencyChanges()
        ).subscribe(new Consumer<CalculateRate.Result>() {
            @Override
            public void accept(CalculateRate.Result result) throws Exception {
                getView().setFromAmount(amountFormat.format(result.convertedAmount));
                getView().setToRate("");
                if (result.currencyFrom.equals(result.currencyTo) || result.rate == 0) {
                    getView().setTitleRate("");
                    getView().setFromRate("");
                } else {
                    getView().setTitleRate(formatRate(result.currencyFrom, result.currencyTo, result.rate));
                    getView().setFromRate(formatRate(result.currencyTo, result.currencyFrom, 1 / result.rate));
                }
            }
        });
    }

    @NonNull
    private Observable<CalculateRate.Result> calculateRate(@NonNull Observable<String> amount,
                                                           @NonNull Observable<Currency> fromCurrency,
                                                           @NonNull Observable<Currency> toCurrency) {
        return calculateRate
                .execute(
                        amount.map(new Function<String, Double>() {
                            @Override
                            public Double apply(String fromValue) throws Exception {
                                if (fromValue.isEmpty()) {
                                    return 0.0;
                                }

                                return numberFormat.parse(fromValue).doubleValue();
                            }
                        }),
                        fromCurrency,
                        toCurrency
                )
                .onErrorResumeNext(new Function<Throwable, ObservableSource<CalculateRate.Result>>() {
                    @Override
                    public ObservableSource<CalculateRate.Result> apply(Throwable throwable) throws Exception {
                        Log.w(ExchangePresenter.class.getName(), throwable.getMessage(), throwable);
                        return Observable.empty();
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @NonNull
    private String formatRate(@NonNull Currency currencyFrom, @NonNull Currency currencyTo, double rate) {
        return currencyFrom.symbol + "1 = " + currencyTo.symbol + rateFormat.format(rate);
    }
}

package com.iismagilov.reactiverates.presenter;

import android.support.annotation.NonNull;

import com.iismagilov.reactiverates.data.RatesDataStorage;
import com.iismagilov.reactiverates.interactor.CalculateRate;

/**
 * Created by Igor Ismagilov on 23.03.17.
 */

public class PresenterManager {

    private static ExchangePresenter exchangePresenter;

    @NonNull
    public static ExchangePresenter getExchangePresenter() {
        if (exchangePresenter == null) {
            exchangePresenter = new ExchangePresenter(new CalculateRate(RatesDataStorage.getInstance()));
        }
        return exchangePresenter;
    }
}

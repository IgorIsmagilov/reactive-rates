package com.iismagilov.reactiverates.presenter;

import android.support.annotation.NonNull;

/**
 * Created by Igor Ismagilov on 23.03.17.
 */

public class BasePresenter<V> {

    private V view;

    public void attachView(@NonNull V view) {
        this.view = view;
    }

    public void detachView() {
        view = null;
    }

    @NonNull
    public V getView() {
        if (view == null) {
            throw new NullPointerException("view reference is null. Have you called attachView()?");
        }
        return view;
    }

}

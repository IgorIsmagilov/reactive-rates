package com.iismagilov.reactiverates.view;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.iismagilov.reactiverates.R;
import com.iismagilov.reactiverates.model.Currency;
import com.jakewharton.rxbinding2.view.RxView;
import com.jakewharton.rxbinding2.widget.RxTextView;

import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;

/**
 * Created by Igor Ismagilov on 25.03.17.
 */

public class CurrencyPagerAdapter extends PagerAdapter {

    private final SparseArray<ViewHolder> viewHolders = new SparseArray<>();

    private final Subject<String> amountChangesSubject = BehaviorSubject.create();

    private final Subject<Boolean> amountFocusChangesSubject = PublishSubject.create();

    private ArrayList<String> savedValues = new ArrayList<>();

    private ArrayList<String> savedRates = new ArrayList<>();

    private Observable<Currency> pageSelectedObservable;

    private static class ViewHolder {
        private final View itemView;

        private final TextView currency;

        private final EditText amount;

        private final TextView rate;

        ViewHolder(View itemView, TextView currency, EditText amount, TextView rate) {
            this.itemView = itemView;
            this.currency = currency;
            this.amount = amount;
            this.rate = rate;
        }
    }

    @Override
    public Object instantiateItem(final ViewGroup container, int position) {
        final int index = getIndex(position);
        ViewHolder holder;
        if ((holder = viewHolders.get(index)) == null) {
            final View view = LayoutInflater.from(container.getContext()).inflate(R.layout.pager_item, container, false);

            holder = new ViewHolder(view, (TextView) view.findViewById(R.id.currency), (EditText) view.findViewById(R.id.amount), (TextView) view.findViewById(R.id.rate));
            holder.currency.setText(Currency.values()[index].toString());

            if (index < savedValues.size()) {
                holder.amount.setTextKeepState(savedValues.get(index));
            }
            if (index < savedRates.size()) {
                holder.rate.setText(savedRates.get(index));
            }
            viewHolders.put(index, holder);

            final ViewHolder finalHolder = holder;
            pageSelectedObservable.subscribe(new Consumer<Currency>() {
                @Override
                public void accept(Currency currency) throws Exception {
                    finalHolder.amount.setTextKeepState(finalHolder.amount.getText());
                }
            });

            RxView.focusChanges(finalHolder.amount).subscribe(amountFocusChangesSubject);

            RxTextView.textChanges(finalHolder.amount)
                      .map(new Function<CharSequence, String>() {
                          @Override
                          public String apply(CharSequence charSequence) throws Exception {
                              return charSequence.toString();
                          }
                      })
                      .filter(new Predicate<String>() {
                          @Override
                          public boolean test(String s) throws Exception {
                              return index == getIndex(((ViewPager) container).getCurrentItem());
                          }
                      })
                      .subscribe(amountChangesSubject);
        }

        if (holder.itemView.getParent() == null) {
            container.addView(holder.itemView);
        } else {
            container.removeView(holder.itemView);
            if (index < ((ViewPager) container).getCurrentItem()) {
                container.addView(holder.itemView, 0);
            } else {
                container.addView(holder.itemView);
            }
        }

        return holder.itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
    }

    @Override
    public int getCount() {
        return Integer.MAX_VALUE;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Parcelable saveState() {
        Bundle bundle = new Bundle();
        bundle.putParcelable("ss", super.saveState());

        ArrayList<String> values = new ArrayList<>();
        int size = viewHolders.size();
        for (int i = 0; i < size; i++) {
            values.add(viewHolders.get(viewHolders.keyAt(i)).amount.getText().toString());
        }
        bundle.putStringArrayList("values", values);

        ArrayList<String> rates = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            rates.add(viewHolders.get(viewHolders.keyAt(i)).rate.getText().toString());
        }
        bundle.putStringArrayList("rates", rates);
        return bundle;
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
        if (state instanceof Bundle) {
            savedValues = ((Bundle) state).getStringArrayList("values");
            savedRates = ((Bundle) state).getStringArrayList("rates");
            super.restoreState(((Bundle) state).getParcelable("ss"), loader);
        } else {
            super.restoreState(state, loader);
        }
    }

    public void setAmount(@NonNull String amount, int position) {
        viewHolders.get(getIndex(position)).amount.setTextKeepState(amount);
    }

    @NonNull
    public Observable<Boolean> valueFocusChanges() {
        return amountFocusChangesSubject;
    }

    public void setRate(@NonNull String rate, int position) {
        viewHolders.get(getIndex(position)).rate.setText(rate);
    }

    public void setPageSelected(@NonNull Observable<Currency> pageSelectedObservable) {
        this.pageSelectedObservable = pageSelectedObservable;
    }

    @NonNull
    public Observable<String> currentAmountChanges() {
        return amountChangesSubject;
    }

    @NonNull
    public Currency getCurrency(int position) {
        return Currency.values()[getIndex(position)];
    }

    public int getMimicCount() {
        return Currency.values().length;
    }

    private int getIndex(int position) {
        return position % getMimicCount();
    }

}

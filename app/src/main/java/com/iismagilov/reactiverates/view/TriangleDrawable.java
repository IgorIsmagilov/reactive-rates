package com.iismagilov.reactiverates.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.iismagilov.reactiverates.R;

/**
 * Created by Igor Ismagilov on 26.03.17.
 */

public class TriangleDrawable extends Drawable {

    private final Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);

    private int dimColor;
    private int triangleColor;

    private Path path = new Path();

    public TriangleDrawable(@NonNull Context context) {
        dimColor = ((ColorDrawable) context.getDrawable(R.color.dim)).getColor();
        triangleColor = ((ColorDrawable) context.getDrawable(R.color.colorPrimary)).getColor();
    }

    @Override
    protected void onBoundsChange(Rect bounds) {
        path.reset();
        path.moveTo(bounds.centerX(), bounds.bottom);
        path.lineTo(bounds.centerX() - bounds.height(), bounds.top);
        path.lineTo(bounds.centerX() + bounds.height(), bounds.top);
        path.close();
    }

    @Override
    public void draw(@NonNull Canvas canvas) {
        canvas.drawColor(dimColor);
        paint.setStyle(Paint.Style.FILL_AND_STROKE);
        paint.setColor(triangleColor);
        canvas.drawPath(path, paint);
    }

    @Override
    public void setAlpha(@IntRange(from = 0, to = 255) int alpha) {
        paint.setAlpha(alpha);
    }

    @Override
    public void setColorFilter(@Nullable ColorFilter colorFilter) {
        paint.setColorFilter(colorFilter);
    }

    @Override
    public int getOpacity() {
        return PixelFormat.UNKNOWN;
    }
}

package com.iismagilov.reactiverates.view;

import android.support.v4.view.ViewPager;

/**
 * Created by Igor Ismagilov on 26.03.17.
 */

public class OnPageChangeListenerAdapter implements ViewPager.OnPageChangeListener {
    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}

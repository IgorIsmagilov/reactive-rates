package com.iismagilov.reactiverates.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.iismagilov.reactiverates.R;
import com.iismagilov.reactiverates.model.Currency;
import com.iismagilov.reactiverates.presenter.ExchangePresenter;
import com.iismagilov.reactiverates.presenter.PresenterManager;

import java.util.concurrent.Callable;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;

public class ExchangeActivity extends AppCompatActivity implements ExchangeView {

    @BindView(R.id.title_rate)
    TextView titleRateView;

    @BindView(R.id.from_pager)
    ViewPager fromPager;

    @BindView(R.id.triangle)
    View triangleView;

    @BindView(R.id.to_pager)
    ViewPager toPager;

    @BindView(R.id.from_pager_page_indicator)
    PageIndicatorView fromPagerPageIndicator;

    @BindView(R.id.to_pager_page_indicator)
    PageIndicatorView toPagerPageIndicator;

    private ExchangePresenter presenter = PresenterManager.getExchangePresenter();

    private Observable<Currency> fromCurrencyChangesObservable;

    private Observable<String> fromAmountChangesObservable;

    private Observable<Boolean> fromAmountFocusChangesObservable;

    private Observable<Currency> toCurrencyChangesObservable;

    private Observable<String> toAmountChangesObservable;

    private Observable<Boolean> toAmountFocusChangesObservable;

    private CurrencyPagerAdapter fromCurrencyPagerAdapter;

    private CurrencyPagerAdapter toCurrencyPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exchange);
        ButterKnife.bind(this);

        triangleView.setBackground(new TriangleDrawable(this));

        fromCurrencyPagerAdapter = new CurrencyPagerAdapter();
        fromAmountFocusChangesObservable = fromCurrencyPagerAdapter.valueFocusChanges();
        fromPager.setOffscreenPageLimit(1);
        fromPager.setAdapter(fromCurrencyPagerAdapter);
        fromCurrencyChangesObservable = createCurrencyChangesObservable(fromPager, fromCurrencyPagerAdapter);
        fromAmountChangesObservable = fromCurrencyPagerAdapter.currentAmountChanges();
        fromCurrencyPagerAdapter.setPageSelected(fromCurrencyChangesObservable);


        toCurrencyPagerAdapter = new CurrencyPagerAdapter();
        toAmountFocusChangesObservable = toCurrencyPagerAdapter.valueFocusChanges();
        toPager.setOffscreenPageLimit(1);
        toPager.setAdapter(toCurrencyPagerAdapter);
        toCurrencyChangesObservable = createCurrencyChangesObservable(toPager, toCurrencyPagerAdapter);
        toAmountChangesObservable = toCurrencyPagerAdapter.currentAmountChanges();
        toCurrencyPagerAdapter.setPageSelected(toCurrencyChangesObservable);

        fromPagerPageIndicator.setIndicators(fromCurrencyPagerAdapter.getMimicCount());
        toPagerPageIndicator.setIndicators(toCurrencyPagerAdapter.getMimicCount());

        fromPager.addOnPageChangeListener(new OnPageChangeListenerAdapter() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                fromPagerPageIndicator.setPosition(position % fromCurrencyPagerAdapter.getMimicCount(), positionOffset);
            }
        });

        toPager.addOnPageChangeListener(new OnPageChangeListenerAdapter() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                toPagerPageIndicator.setPosition(position % toCurrencyPagerAdapter.getMimicCount(), positionOffset);
            }
        });

        if (savedInstanceState == null) {
            fromPager.setCurrentItem(fromPager.getAdapter().getCount() / 2);
            toPager.setCurrentItem(toPager.getAdapter().getCount() / 2 + 1);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.attachView(this);
    }

    @Override
    protected void onStop() {
        presenter.detachView();
        super.onStop();
    }

    @NonNull
    @Override
    public Observable<Currency> fromCurrencyChanges() {
        return fromCurrencyChangesObservable;
    }

    @NonNull
    @Override
    public Observable<String> fromAmountChanges() {
        return fromAmountChangesObservable;
    }

    @NonNull
    @Override
    public Observable<Boolean> fromAmountFocusChanges() {
        return fromAmountFocusChangesObservable;
    }

    @NonNull
    @Override
    public Observable<Currency> toCurrencyChanges() {
        return toCurrencyChangesObservable;
    }

    @NonNull
    @Override
    public Observable<String> toAmountChanges() {
        return toAmountChangesObservable;
    }

    @NonNull
    @Override
    public Observable<Boolean> toAmountFocusChanges() {
        return toAmountFocusChangesObservable;
    }

    @Override
    public void setFromAmount(@NonNull String fromAmount) {
        ((CurrencyPagerAdapter) fromPager.getAdapter()).setAmount(fromAmount, fromPager.getCurrentItem());
    }

    @Override
    public void setToAmount(@NonNull String toAmount) {
        ((CurrencyPagerAdapter) toPager.getAdapter()).setAmount(toAmount, toPager.getCurrentItem());
    }

    @Override
    public void setFromRate(@NonNull String fromRate) {
        ((CurrencyPagerAdapter) fromPager.getAdapter()).setRate(fromRate, fromPager.getCurrentItem());
    }

    @Override
    public void setToRate(@NonNull String rate) {
        ((CurrencyPagerAdapter) toPager.getAdapter()).setRate(rate, toPager.getCurrentItem());
    }

    @Override
    public void setTitleRate(@NonNull String titleRate) {
        titleRateView.setText(titleRate);
    }


    @NonNull
    private Observable<Currency> createCurrencyChangesObservable(@NonNull final ViewPager viewPager,
                                                                 @NonNull final CurrencyPagerAdapter adapter) {
        return Observable.create(new ObservableOnSubscribe<Currency>() {
            @Override
            public void subscribe(final ObservableEmitter<Currency> e) throws Exception {
                viewPager.addOnPageChangeListener(new OnPageChangeListenerAdapter() {
                    @Override
                    public void onPageSelected(int position) {
                        e.onNext(adapter.getCurrency(position));
                    }
                });
            }
        }).share().startWith(Observable.fromCallable(new Callable<Currency>() {
            @Override
            public Currency call() throws Exception {
                return adapter.getCurrency(viewPager.getCurrentItem());
            }
        }));
    }
}

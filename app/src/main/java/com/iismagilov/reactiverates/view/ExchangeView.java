package com.iismagilov.reactiverates.view;

import android.support.annotation.NonNull;

import com.iismagilov.reactiverates.model.Currency;

import io.reactivex.Observable;

/**
 * Created by Igor Ismagilov on 23.03.17.
 */

public interface ExchangeView {

    @NonNull
    Observable<Currency> fromCurrencyChanges();

    @NonNull
    Observable<String> fromAmountChanges();

    @NonNull
    Observable<Boolean> fromAmountFocusChanges();

    @NonNull
    Observable<Currency> toCurrencyChanges();

    @NonNull
    Observable<String> toAmountChanges();

    @NonNull
    Observable<Boolean> toAmountFocusChanges();

    void setFromAmount(@NonNull String fromAmount);

    void setFromRate(@NonNull String fromRate);

    void setToAmount(@NonNull String toAmount);

    void setToRate(@NonNull String toRate);

    void setTitleRate(@NonNull String titleRate);
}

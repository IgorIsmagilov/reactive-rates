package com.iismagilov.reactiverates.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.ColorInt;
import android.support.annotation.Dimension;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import com.iismagilov.reactiverates.R;

/**
 * Created by Igor Ismagilov on 26.03.17.
 */

public class PageIndicatorView extends View {

    private static final int DEFAULT_INDICATOR_COLOR = Color.parseColor("#4DFFFFFF");

    private static final int DEFAULT_CURRENT_INDICATOR_COLOR = Color.WHITE;

    @ColorInt
    int indicatorColor = DEFAULT_INDICATOR_COLOR;

    @ColorInt
    int currentIndicatorColor = DEFAULT_CURRENT_INDICATOR_COLOR;

    @Dimension
    int indicatorRadius;

    int indicators;

    private final Paint indicatorPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

    public PageIndicatorView(Context context) {
        this(context, null);
    }

    public PageIndicatorView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PageIndicatorView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public PageIndicatorView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

        final TypedArray a = context.obtainStyledAttributes(
                attrs, R.styleable.PageIndicatorView, defStyleAttr, defStyleRes);

        int size = a.getIndexCount();
        for (int i = 0; i < size; i++) {
            int attr = a.getIndex(i);
            switch (attr) {
                case R.styleable.PageIndicatorView_indicator_color:
                    indicatorColor = a.getColor(attr, DEFAULT_INDICATOR_COLOR);
                    break;
                case R.styleable.PageIndicatorView_current_indicator_color:
                    currentIndicatorColor = a.getColor(attr, DEFAULT_CURRENT_INDICATOR_COLOR);
                    break;
                case R.styleable.PageIndicatorView_indicator_radius:
                    indicatorRadius = a.getDimensionPixelSize(attr, 0);
                    break;
            }
        }
    }

    public void setIndicators(int indicators) {
        this.indicators = indicators;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if (isInEditMode()) {
            setMeasuredDimension(100, 100);
        } else {
            int width = (3 * indicators - 1) * indicatorRadius;
            int height = 2 * indicatorRadius;
            setMeasuredDimension(width, height);
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        indicatorPaint.setColor(indicatorColor);
        for (int i = 0; i < indicators; i++) {
            canvas.drawCircle(indicatorRadius * (i * 3 + 1), canvas.getHeight() / 2, indicatorRadius, indicatorPaint);
        }

        indicatorPaint.setColor(currentIndicatorColor);
        float currentIndicatorX = ((position * 3 + 1) * indicatorRadius) + 3 * positionOffset * indicatorRadius;
        currentIndicatorX = Math.min(canvas.getWidth() - indicatorRadius, currentIndicatorX);
        canvas.drawCircle(currentIndicatorX, canvas.getHeight() / 2, indicatorRadius, indicatorPaint);
    }

    int   position;
    float positionOffset;

    public void setPosition(int position, float positionOffset) {
        this.position = position;
        this.positionOffset = positionOffset;
        invalidate();
    }
}

package com.iismagilov.reactiverates.data.local;

import android.support.annotation.NonNull;

import com.iismagilov.reactiverates.model.Currency;
import com.iismagilov.reactiverates.model.Rates;

import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;

import io.reactivex.Maybe;
import io.reactivex.Observable;

/**
 * Created by Igor Ismagilov on 25.03.17.
 */

public class RatesMemoryStorage {

    private final ConcurrentHashMap<Currency, Rates> ratesMap = new ConcurrentHashMap<>();

    @NonNull
    public Observable<Rates> getRates(@NonNull final Currency currency) {
        return Maybe.fromCallable(new Callable<Rates>() {
            @Override
            public Rates call() throws Exception {
                return ratesMap.get(currency);
            }
        }).toObservable();
    }

    public void put(@NonNull Currency currency, @NonNull Rates rates) {
        ratesMap.put(currency, rates);
    }

}

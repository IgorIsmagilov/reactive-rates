package com.iismagilov.reactiverates.data.remote;

import android.support.annotation.NonNull;

import com.iismagilov.reactiverates.model.Rates;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Igor Ismagilov on 23.03.17.
 */

public interface FixerService {

    @NonNull
    @GET("latest")
    Observable<Rates> latest(@Query("base") String base);

}

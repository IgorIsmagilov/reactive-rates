package com.iismagilov.reactiverates.data;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.iismagilov.reactiverates.data.local.RatesMemoryStorage;
import com.iismagilov.reactiverates.data.remote.FixerService;
import com.iismagilov.reactiverates.model.Currency;
import com.iismagilov.reactiverates.model.Rates;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Igor Ismagilov on 25.03.17.
 */

public class RatesDataStorage {

    private static RatesDataStorage instance;

    private final FixerService fixerService;

    private final RatesMemoryStorage ratesMemoryStorage = new RatesMemoryStorage();

    public static RatesDataStorage getInstance() {
        if (instance == null) {
            instance = new RatesDataStorage();
        }
        return instance;
    }

    private RatesDataStorage() {
        final HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BASIC);
        final OkHttpClient httpClient = new OkHttpClient.Builder().addInterceptor(loggingInterceptor)
                                                                  .connectTimeout(5, TimeUnit.SECONDS)
                                                                  .build();
        final Gson gson = new GsonBuilder().registerTypeAdapter(Rates.class, new RatesTypeAdapter()).create();
        final Retrofit retrofit = new Retrofit.Builder().baseUrl("http://api.fixer.io/")
                                                        .addConverterFactory(GsonConverterFactory.create(gson))
                                                        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                                                        .client(httpClient)
                                                        .build();
        fixerService = retrofit.create(FixerService.class);
    }

    public Observable<Rates> getRates(@NonNull Observable<Currency> baseCurrency) {
        return baseCurrency.switchMap(new Function<Currency, ObservableSource<Rates>>() {
            @Override
            public ObservableSource<Rates> apply(final Currency currency) throws Exception {
                return fixerService.latest(currency.name())
                                   .doOnNext(new Consumer<Rates>() {
                                       @Override
                                       public void accept(Rates rates) throws Exception {
                                           ratesMemoryStorage.put(currency, rates);
                                       }
                                   })
                                   .onErrorResumeNext(new Function<Throwable, ObservableSource<? extends Rates>>() {
                                       @Override
                                       public ObservableSource<? extends Rates> apply(final Throwable throwable) throws Exception {
                                           final Rates rates = new Rates();
                                           rates.setBase(currency);
                                           return ratesMemoryStorage.getRates(currency)
                                                                    .switchIfEmpty(Observable.just(rates)).concatWith(Observable.<Rates>error(throwable));
                                       }
                                   })
                                   .retryWhen(new Function<Observable<Throwable>, ObservableSource<?>>() {
                                       @Override
                                       public ObservableSource<?> apply(Observable<Throwable> throwableObservable) throws Exception {
                                           return throwableObservable.delay(3, TimeUnit.SECONDS);
                                       }
                                   })
                                   .repeatWhen(new Function<Observable<Object>, ObservableSource<?>>() {
                                       @Override
                                       public ObservableSource<?> apply(Observable<Object> objectObservable) throws Exception {
                                           return Observable.interval(0, 30, TimeUnit.SECONDS);
                                       }
                                   })
                                   .startWith(ratesMemoryStorage.getRates(currency))
                                   .subscribeOn(Schedulers.io());
            }
        });
    }
}

package com.iismagilov.reactiverates.data;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import com.iismagilov.reactiverates.model.Currency;
import com.iismagilov.reactiverates.model.Rates;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Igor Ismagilov on 26.03.17.
 */

class RatesTypeAdapter extends TypeAdapter<Rates> {

    @Override
    public void write(JsonWriter out, Rates value) throws IOException {
        throw new UnsupportedOperationException("Serialization to JSON is not supported");
    }

    @Override
    public Rates read(JsonReader in) throws IOException {
        in.beginObject();

        final Rates rates = new Rates();

        while (in.hasNext()) {
            final String name = in.nextName();
            if ("base".equals(name)) {
                rates.setBase(Currency.valueOf(in.nextString()));
            } else if ("rates".equals(name)) {
                rates.setRates(readRates(in));
            } else {
                in.skipValue();
            }
        }

        in.endObject();

        return rates;
    }

    private Map<Currency, Double> readRates(JsonReader in) throws IOException {
        in.beginObject();

        final Map<Currency, Double> rates = new HashMap<>();

        while (in.hasNext()) {
            final String currencyName = in.nextName();

            try {
                final Currency currency = Currency.valueOf(currencyName);
                rates.put(currency, in.nextDouble());
            } catch (IllegalArgumentException e) {
                in.nextDouble();
            }
        }

        in.endObject();

        return rates;
    }
}

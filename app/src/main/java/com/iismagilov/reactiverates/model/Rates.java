package com.iismagilov.reactiverates.model;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Igor Ismagilov on 23.03.17.
 */

public class Rates {

    private Currency base;

    private Map<Currency, Double> rates = new HashMap<>();

    @Nullable
    public Currency getBase() {
        return base;
    }

    public void setBase(@NonNull Currency base) {
        this.base = base;
    }

    @NonNull
    public Map<Currency, Double> getRates() {
        return rates;
    }

    public void setRates(@NonNull Map<Currency, Double> rates) {
        this.rates = rates;
    }
}

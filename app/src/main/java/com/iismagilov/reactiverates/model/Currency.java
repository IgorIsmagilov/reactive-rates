package com.iismagilov.reactiverates.model;

import android.support.annotation.NonNull;

/**
 * Created by Igor Ismagilov on 26.03.17.
 */

public enum Currency {
    USD("$"), EUR("€"), GBP("£");

    @NonNull
    public final String symbol;

    Currency(@NonNull String symbol) {
        this.symbol = symbol;
    }
}
